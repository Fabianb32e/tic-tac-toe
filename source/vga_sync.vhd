--------------------------------------------------------------------------------
--
--	File Name: vga.vhd
--
-- VGA output: 1152 x 864 @ 60 Hz; 81.62 MHz pixel frequency
--
--	Requirements:
--	- Needs a 81.62 MHz on vga_clock
--
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- Libraries
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;

--------------------------------------------------------------------------------
-- Entity
--------------------------------------------------------------------------------
entity vga_sync is
	generic(	hor_front_porch 	: integer  	:= 	64;									-- horizontal front porch
				hor_sync_pulse  	: integer	:=	  120;									-- horizontal sync pulse
				hor_back_porch		: integer	:=	  184;									-- horizontal back porch
				hor_polarity		: std_logic :=   '0';									-- horizontal polarity
				hor_tot_pixel		: integer	:=  1520;									-- horizontal total of pixels
				
				ver_front_porch 	: integer	:=     1;									-- vertical front porch
				ver_sync_pulse		: integer	:=		 3;									-- vertical sync pulse
				ver_back_porch		: integer	:=		27;									-- vertical back porch
				ver_polarity		: std_logic :=	  '1';									-- vertical polarity
				ver_tot_pixel		: integer	:=  895);									-- vertical total of pixels
				
	port(		vga_clock			: in	std_logic;											-- vga clock input
				reset					: in 	std_logic;											-- reset (active high --> reset)
				hsync					: out	std_logic;											-- horizontal sync
				vsync					: out std_logic;											-- vertical sync
				xpos					: out integer 		range 0 to hor_tot_pixel;		-- x position of the vga image area
				ypos 					: out integer 		range 0 to ver_tot_pixel;		-- y position of the vga image area
				video_on				: out std_logic);											-- 1 = x and y position are inside of the image area
end vga_sync;

--------------------------------------------------------------------------------
-- Architecture
--------------------------------------------------------------------------------
architecture Beh_VGA of vga_sync is
	
	constant		hor_porch		: integer range 0 to hor_front_porch + hor_sync_pulse + hor_back_porch := hor_front_porch + hor_sync_pulse + hor_back_porch;		-- total horizontal porch
	constant 	ver_porch		: integer range 0 to ver_front_porch + ver_sync_pulse + ver_back_porch := ver_front_porch + ver_sync_pulse + ver_back_porch;		-- total vertical   porch

	signal 		hor_counter		: integer range 0 to hor_tot_pixel 	:= 0;			-- horizontal counter
	signal 		ver_counter		: integer range 0 to ver_tot_pixel	:= 0;			-- vertical   counter

begin
	process(all)
	begin
	
		if rising_edge(vga_clock) then
			
			-- horizontal -----------------------------------------------------
			hor_counter 	<= hor_counter + 1;
			hsync 			<= not hor_polarity;
			
			if hor_counter > hor_front_porch and hor_counter < hor_front_porch + hor_sync_pulse then
				hsync <= hor_polarity;
			end if;
			
			if hor_counter = hor_tot_pixel - 1 then
				hor_counter <= 0;
				ver_counter <= ver_counter + 1;
			end if;
			
			-- vertical -------------------------------------------------------
			vsync <= not ver_polarity;
			
			if ver_counter > ver_front_porch and ver_counter < ver_front_porch + ver_sync_pulse then
				vsync <= ver_polarity;
			end if;
			
			if ver_counter = ver_tot_pixel - 1 then
				ver_counter <= 0;
			end if;
			
			-- reset ----------------------------------------------------------
			if reset = '1' then
				xpos 		<= 0;
				ypos		<= 0;
				video_on	<= '0';
				hsync 	<= not hor_polarity;
				vsync 	<= not ver_polarity;
			end if;
			
		end if;
		
		-- x position --------------------------------------------------------
		xpos <= 0;
		if hor_counter >= hor_porch then
			xpos <= hor_counter - hor_porch;
		end if;
		
		-- y position --------------------------------------------------------
		ypos <= 0;
		if ver_counter >= ver_porch then
			ypos <= ver_counter - ver_porch;
		end if;
		
		-- video on ----------------------------------------------------------
		video_on <= '0';
		if hor_counter > hor_porch and ver_counter > ver_porch then
			video_on <= '1';
		end if;
	
	end process;
	
end architecture;


-- <-- end of file -->