--------------------------------------------------------------------------------
--
--	File Name: tictactoe.vhd
--
--	TOP Entity of the Tic-Tac-Toe Game
--
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- Libraries
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use work.my_package.all;

--------------------------------------------------------------------------------
-- Entity
--------------------------------------------------------------------------------
entity tictactoe is
	generic(	vga_color_depth	: integer	:= 4);
	
	port(		clk_50		: in 	std_logic;																			-- global 50 MHz clock
				reset			: in  std_logic;																			-- Reset
				vga_hsync	: out std_logic;																			-- vga horizontal sync
				vga_vsync	: out std_logic;																			-- vga vertical sync
				vga_red		: out std_logic_vector(vga_color_depth - 1 downto 0) := x"0";				-- vga channel red
				vga_green	: out std_logic_vector(vga_color_depth - 1 downto 0) := x"0";				-- vga channel green
				vga_blue		: out std_logic_vector(vga_color_depth - 1 downto 0) := x"0";				-- vga channel blue
				ps2_clk		: in	std_logic;																			-- ps2 clock
				ps2_data		: in	std_logic;																			-- ps2 data
				led 			: out std_LOGIC_VECTOR(7 downto 0) := x"00";										-- ps2 data blink
				led_new		: out std_LOGIC);																			-- ps2 new data
end tictactoe;

--------------------------------------------------------------------------------
-- Architecture
--------------------------------------------------------------------------------
architecture Beh_TicTacToe of tictactoe is

	-- PLL component --------------------------------------------------------
	component pll is
		port(	inclk0	: IN  STD_LOGIC  := '0';
				c0			: OUT STD_LOGIC);
	end component pll;

	-- Graphics component ---------------------------------------------------
	component graphic is
		port(	clk					: in  std_logic;																	-- clock
				reset					: in  std_logic;																	-- Reset
				state					: in  state_graphics;															-- state to change state machine
				cursor_pos			: in 	state_cursor_pos;															-- state of cursor position
				cursor_col			: in 	state_cursor_color;														-- state of cursor color
				field_p1				: in  field_player;																-- player 1 fields
				field_p2				: in 	field_player;																-- player 2 fields
				current_player		: in 	player_turn;																-- state of which players turn
				winner				: in  integer range 0 to 4	:= 0;												-- winner = 0 -> no winner, winner = 1 -> player 1, winner = 2 -> player 2, winner = 3 -> draw
				gamemode				: in  game_state;																	-- gamemode to know, if 1 Player or 2 Player mode was selected
				vga_hsync			: out std_logic	:= '0';														-- vga horizontal sync
				vga_vsync			: out std_logic	:= '0';														-- vga vertical   sync
				vga_red				: out std_logic_vector(vga_color_depth - 1 downto 0) := x"0";		-- vga red   channel
				vga_green			: out std_logic_vector(vga_color_depth - 1 downto 0) := x"0";		-- vga green channel
				vga_blue				: out std_logic_vector(vga_color_depth - 1 downto 0) := x"0";		-- vga blue  channel
				allow_update		: out std_logic	:= '0');														-- flag to allow game updates
	end component graphic;
	
	-- PS2 component --------------------------------------------------------
	component ps2 is
		port(	clk          		: IN  STD_LOGIC;                     										--system clock
				ps2_clk      		: IN  STD_LOGIC;                     										--clock signal from PS/2 keyboard
				ps2_data     		: IN  STD_LOGIC;                     										--data signal from PS/2 keyboard
				ps2_code_new 		: OUT STD_LOGIC;                     										--flag that new PS/2 code is available on ps2_code bus
				ps2_code     		: OUT STD_LOGIC_VECTOR(7 DOWNTO 0)); 										--code received from PS/2
	end component ps2;
	 
	-- Game component -------------------------------------------------------
	component gamerule is
		port( reset					: in	std_logic;																	-- reset
				sw_reset				: in  std_logic;																	-- Software reset
				clk					: in	std_logic;																	-- global 81.62 MHz clock 
				winner				: out integer range 0 to 4	:= 0;												-- winner = 0 -> no winner, winner = 1 -> player 1, winner = 2 -> player 2, winner = 3 -> draw
				cursor_x_pos		: in 	integer range 0 to 2	:= 0;												-- cursor x-position
				cursor_y_pos		: in 	integer range 0 to 2	:= 0; 											-- cursor y-position
				place_symbol		: in  std_logic;																	-- flag to know, if the symbol should be placed
				gamemode				: in  game_state;																	-- gamemode to know, if 1 Player or 2 Player mode was selected
				field_p1				: out field_player	:= (others => (others => '0'));					-- player 1 fields
				field_p2				: out field_player	:= (others => (others => '0'));					-- player 2 fields
				cursor_color		: out state_cursor_color;														-- cursor color
				current_player		: out	player_turn;																-- state of the current player turn
				cursor_pos			: out	state_cursor_pos);														 -- state of cursor position
	end component gamerule;
	
	-- Signals --------------------------------------------------------------
	signal reset_sync_0					: std_logic						:= '0';
	signal reset_synced					: std_logic						:= '0';
	
	signal software_reset				: std_logic						:= '0';
	
	signal state_of_game 				: game_state					:= Init;
	signal state_of_graphics			: state_graphics				:= Start_Menu;
	signal cursor_position				: state_cursor_pos			:= A0;
	signal cursor_color					: state_cursor_color			:= red;
	
	signal cursor_x_pos					: integer range 0 to 2		:= 0;
	signal cursor_y_pos					: integer range 0 to 2		:= 0;
	
	signal place_symbol					: std_logic						:= '0';
	
	signal allow_update					: std_logic;
	
	signal ps2_key							: std_LOGIC_VECTOR(7 DOWNTO 0);
	signal ps2_new							: std_LOGIC;
	
	signal clk_81 							: std_logic;
	
	signal field_player_1 				: field_player;
	signal field_player_2				: field_player;
	
	signal current_player				: player_turn;
	
	signal winner							: integer range 0 to 4		:= 0;
	
	-- Constants ------------------------------------------------------------
	-- Number Buttons ---------------------------------------------------
	constant numpad_1						: std_logic_vector(7 downto 0) := x"69";
	constant numpad_2						: std_logic_vector(7 downto 0) := x"72";
	constant button_1						: std_logic_vector(7 downto 0) := x"16";
	constant button_2						: std_logic_vector(7 downto 0) := x"1E";
	
	-- Arrow Keys -------------------------------------------------------
	constant button_up					: std_logic_vector(7 downto 0) := x"75";
	constant button_down					: std_logic_vector(7 downto 0) := x"72";
	constant button_left					: std_logic_vector(7 downto 0) := x"6B";
	constant button_right				: std_logic_vector(7 downto 0) := x"74";
	
	constant button_enter				: std_logic_vector(7 downto 0) := x"5A";
	constant button_escape				: std_logic_vector(7 downto 0) := x"76";
	
begin
	
	-- PLL ------------------------------------
	my_pll : pll
	port map(	inclk0 				=> clk_50,
					c0						=> clk_81);
					
	-- Graphics -------------------------------
	my_graphics : graphic
	port map(	clk					=> clk_81,
					reset					=> reset_synced,
					state					=> state_of_graphics,
					cursor_pos			=> cursor_position,
					cursor_col			=> cursor_color,
					field_p1				=> field_player_1,
					field_p2				=> field_player_2,
					current_player 	=> current_player,
					winner				=> winner,
					gamemode				=> state_of_game,
					vga_hsync			=> vga_hsync,
					vga_vsync			=> vga_vsync,
					vga_red				=> vga_red,
					vga_green			=> vga_green,
					vga_blue				=> vga_blue,
					allow_update		=> allow_update);
					
	-- PS2 Keyboard ---------------------------
	my_ps2_keyboard : ps2
	port map(	clk          		=> clk_81,
					ps2_clk      		=> ps2_clk,
					ps2_data     		=> ps2_data,
					ps2_code_new 		=> ps2_new,
					ps2_code     		=> ps2_key);
				
	-- Game Logic -----------------------------
	my_gamerule : gamerule
	port map(	reset					=> reset_synced,
					sw_reset				=> software_reset,
					clk					=> clk_81,
					winner				=> winner,
					cursor_x_pos		=> cursor_x_pos,
					cursor_y_pos 		=> cursor_y_pos,
					place_symbol		=> place_symbol,
					gamemode				=> state_of_game,
					field_p1				=> field_player_1,
					field_p2				=> field_player_2,
					cursor_color		=> cursor_color,
					current_player		=> current_player,
					cursor_pos			=> cursor_position);
	
	-- Main Process --------------------------------------------
	Main:
	process(all)
	begin
		
		if rising_edge(clk_81) then
		
			-- Reset Sync ------------------------------------
			reset_sync_0 	<= not reset;
			reset_synced 	<= reset_sync_0;
			
			software_reset <= '0';
			
			-- Game ------------------------------------------
			case state_of_game is
				-- Init -----------------------------------
				when Init =>
				-- initialize the game ----------------
					cursor_x_pos	<= 0;
					cursor_y_pos	<= 0;
					place_symbol	<= '0';
					
					software_reset <= '1';
					
					if allow_update = '1' then
						state_of_game <= Start_Menu;					-- switch to the game state start menu
					end if;
				
				-- Start Menu -----------------------------
				when Start_Menu =>
				
					if allow_update = '1' then
						state_of_graphics <= Start_Menu;				-- display start menu
					end if;
					
					-- 1 Player Mode select
					if  ps2_new = '1' and 
						(ps2_key = numpad_1 or ps2_key = button_1) then		-- Button 1
						state_of_game <= Game_1_Player;		-- 
					end if;
					
					-- 2 Player Mode select
					if  ps2_new = '1' and 
						(ps2_key = numpad_2 or ps2_key = button_2) then		-- Button 2
						state_of_game <= Game_2_Player;
					end if;
					
				-- 1 Player Game --------------------------
				when Game_1_Player =>
				
					if allow_update = '1' then
						state_of_graphics <= Game;						-- display the game
					end if;
					
					-- Enter / to set symbol to field ------
					if ps2_key = button_enter then
						place_symbol <= '1';
					else
						place_symbol <= '0';
					end if;
					
					if winner /= 0 then
						state_of_game <= Winner_Screen;
					end if;
				
				-- 2 Player Game --------------------------
				when Game_2_Player =>
				
					if allow_update = '1' then
						state_of_graphics <= Game;						-- display the game
					end if;
					
					-- Enter / to set symbol to field ------
					if ps2_key = button_enter then
						place_symbol <= '1';
					else
						place_symbol <= '0';
					end if;
					
					if winner /= 0 then
						state_of_game <= Winner_Screen;
					end if;
					
				-- Winner Screen --------------------------
				when Winner_Screen =>
					if allow_update = '1' then
						state_of_graphics <= Winner_Screen;			-- display the Winner_Screen
					end if;
					
					if ps2_new = '1' and ps2_key = button_enter then		-- Enter key pressed
						software_reset <= '1';
						state_of_game 	<= Init;
					end if;
				
			end case;
			
			-- Keyboard --------------------------------------
			if ps2_new = '1' then										-- check for flag of new keyboard input 
				
				-- In Game Kexboard mapping -----------------
				if state_of_game = Game_1_Player or state_of_game = Game_2_Player then
					-- Arrow left / Numblock 4 -------------
					if ps2_key = button_left then
						if cursor_x_pos > 0 then
							cursor_x_pos <= cursor_x_pos -1;
						end if;
					end if;
					
					-- Arrow right / Numblock 6 ------------
					if ps2_key = button_right then
						if cursor_x_pos < 2 then
							cursor_x_pos <= cursor_x_pos +1;
						end if;
					end if;
					
					-- Arrow up / Numblock 8 ---------------
					if ps2_key = button_up then
						if cursor_y_pos > 0 then
							cursor_y_pos <= cursor_y_pos -1;
						end if;
					end if;
					
					-- Arrow down / Numblock 4 -------------
					if ps2_key = button_down then
						if cursor_y_pos < 2 then
							cursor_y_pos <= cursor_y_pos +1;
						end if;
					end if;
					
					-- Escape / to reset cursor ------------
					if ps2_key = button_escape then
						cursor_x_pos <= 0;
						cursor_y_pos <= 0;
					end if;
					
				end if; -- Game_1_Player or Game_2_Player
			end if; -- Keyboard
			
			-- Reset -----------------------------------------
			if reset_synced = '1' then
				state_of_game	<= Init;
			end if;
						
		end if; -- rising_edge(clk_81)
		
	end process Main;
	
	led_new 	<= ps2_new;		-- for debugging
	led 		<= ps2_key;		-- for debugging
	
end architecture;

-- <-- end of file -->