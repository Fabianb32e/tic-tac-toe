--------------------------------------------------------------------------------
--
--	File Name: gamerule.vhd
--
--	This vhdl code checks if a player has 3 symbols of the game in a row.
--
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- Libraries
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use work.my_package.all;

--------------------------------------------------------------------------------
-- Entity
--------------------------------------------------------------------------------
entity gamerule is
	--generic();
	
	port(		reset					: in	std_logic;														-- Hardware reset
				sw_reset				: in  std_logic;														-- Software reset
				clk					: in	std_logic;														-- global 81.62 MHz clock 
				winner				: out integer range 0 to 4	:= 0;									-- winner = 0 -> no winner, winner = 1 -> player 1, winner = 2 -> player 2, winner = 3 -> PC, winner = 4 -> draw
				cursor_x_pos		: in 	integer range 0 to 2	:= 0;									-- cursor x-position
				cursor_y_pos		: in 	integer range 0 to 2	:= 0; 								-- cursor y-position
				place_symbol		: in  std_logic;														-- flag to know, if the symbol should be placed
				gamemode				: in  game_state;														-- gamemode to know, if 1 Player or 2 Player mode was selected
				field_p1				: out field_player	:= (others => (others => '0'));		-- player 1 fields
				field_p2				: out field_player	:= (others => (others => '0'));		-- player 2 fields
				cursor_color		: out state_cursor_color;											-- cursor color
				current_player		: out	player_turn;													-- state of the current player turn
				cursor_pos			: out	state_cursor_pos);											-- state of cursor position
end gamerule;

--------------------------------------------------------------------------------
-- Architecture
--------------------------------------------------------------------------------
architecture Beh_Gamerule of gamerule is

	signal field_player_1 		: field_player 		:= (others => (others => '0'));
	signal field_player_2		: field_player			:= (others => (others => '0'));
	
	signal cursor_position		: state_cursor_pos	:= A0;
	
	signal allow_placement		: std_logic				:= '0';
	signal active_player			: std_logic				:= '0';
	
	signal state_player_turn	: player_turn			:= Player_1_Turn;
	
	signal found_move 			: std_logic				:= '0';

begin
	
Process_Game:
process(all)
	-- Variables --------------------------------------------------------
	variable found_move : boolean := false;
	
begin
	if rising_edge(clk) then
	
		current_player <= state_player_turn;
	
		-- Reset ---------------------------------------------------------
		if reset = '1' or sw_reset = '1' then
			field_player_1 	<= (others => (others => '0'));	-- Reset Player 1 field
			field_player_2 	<= (others => (others => '0'));	-- Reset Player 2 field
			allow_placement	<= '0';
			state_player_turn <= Player_1_Turn;						-- Reset Player Turn to Player 1
			winner				<= 0;
		end if;
		
		-- Check for curosr color and to allow placment ------------------
		-- allowed ---------------------------------------------------
		if field_player_1(cursor_x_pos, cursor_y_pos) = '0' and 
			field_player_2(cursor_x_pos, cursor_y_pos) = '0' then
			cursor_color 		<= green;	-- cursor color set to green
			allow_placement	<= '1';		-- placement is allowed
			
		-- not allowed -----------------------------------------------
		else
			cursor_color 		<= red;		-- cursor color set to red
			allow_placement	<= '0';		-- placement is not allowed
		end if;
		
		-- Placement -----------------------------------------------------
		found_move := false;
		
		if allow_placement = '1' then
			case state_player_turn is
				-- Player 1 Turn ---------------------------------------
				when Player_1_Turn =>
					if place_symbol = '1' then
						field_player_1(cursor_x_pos ,cursor_y_pos) <= '1';
						-- 2 Player Mode selected --------------------
						if gamemode = Game_2_Player then
							state_player_turn <= Player_2_Turn;
							allow_placement	<= '0';
							
						-- 1 Player Mode selected --------------------
						elsif gamemode = Game_1_Player then
							state_player_turn <= PC_Turn;
							
						end if;
					end if;
				-- Player 2 Turn ---------------------------------------
				when Player_2_Turn =>
					if place_symbol = '1' then
						field_player_2(cursor_x_pos ,cursor_y_pos) <= '1';
						state_player_turn <= Player_1_Turn;
						allow_placement	<= '0';
					end if;
				-- PC Turn ---------------------------------------------
				when PC_Turn		 =>
				
					-- Check for own potential wins in rows ---------
					 for i in 0 to 2 loop
						if not found_move then
							 if (field_player_2(i, 0) = '1' and field_player_2(i, 1) = '1' and 
								  field_player_2(i, 2) = '0' and field_player_1(i, 2) = '0') then
								  field_player_2(i, 2) <= '1';
								  found_move := true;
								  
							 elsif (field_player_2(i, 0) = '1' and field_player_2(i, 2) = '1' and 
									  field_player_2(i, 1) = '0' and field_player_1(i, 1) = '0') then
								  field_player_2(i, 1) <= '1';
								  found_move := true;
								  
							 elsif (field_player_2(i, 1) = '1' and field_player_2(i, 2) = '1' and 
									  field_player_2(i, 0) = '0' and field_player_1(i, 0) = '0') then
								  field_player_2(i, 0) <= '1';
								  found_move := true;
							 end if;
						end if;
					end loop;
					
					-- Check for own potential wins in columns ------
					 for i in 0 to 2 loop
						if not found_move then
							 if (field_player_2(0, i) = '1' and field_player_2(1, i) = '1' 
								  and field_player_2(2, i) = '0' and field_player_1(2, i) = '0') then
								  field_player_2(2, i) <= '1';
								  found_move := true;
								  
							 elsif (field_player_2(0, i) = '1' and field_player_2(2, i) = '1' and 
									  field_player_2(1, i) = '0' and field_player_1(1, i) = '0') then
								  field_player_2(1, i) <= '1';
								  found_move := true;
								  
							 elsif (field_player_2(1, i) = '1' and field_player_2(2, i) = '1' and 
									  field_player_2(0, i) = '0' and field_player_1(0, i) = '0') then
								  field_player_2(0, i) <= '1';
								  found_move := true;
							 end if;
						end if;
					end loop;
					
					-- Check for own potential wins in diagonals ----
					if not found_move then
						if (field_player_2(0, 0) = '1' and field_player_2(1, 1) = '1' and field_player_2(2, 2) = '0' and field_player_1(2, 2) = '0') then
							 field_player_2(2, 2) <= '1';
							 found_move := true;
							 
						elsif (field_player_2(0, 0) = '1' and field_player_2(2, 2) = '1' and field_player_2(1, 1) = '0' and field_player_1(1, 1) = '0') then
							 field_player_2(1, 1) <= '1';
							 found_move := true;
							 
						elsif (field_player_2(1, 1) = '1' and field_player_2(2, 2) = '1' and field_player_2(0, 0) = '0' and field_player_1(0, 0) = '0') then
							 field_player_2(0, 0) <= '1';
							 found_move := true;
							 
						elsif (field_player_2(0, 2) = '1' and field_player_2(1, 1) = '1' and field_player_2(2, 0) = '0' and field_player_1(2, 0) = '0') then
							 field_player_2(2, 0) <= '1';
							 found_move := true;
							 
						elsif (field_player_2(0, 2) = '1' and field_player_2(2, 0) = '1' and field_player_2(1, 1) = '0' and field_player_1(1, 1) = '0') then
							 field_player_2(1, 1) <= '1';
							 found_move := true;
							 
						elsif (field_player_2(1, 1) = '1' and field_player_2(2, 0) = '1' and field_player_2(0, 2) = '0' and field_player_1(0, 2) = '0') then
							 field_player_2(0, 2) <= '1';
							 found_move := true;
						end if;
					end if;
					
					
					-- Check for horizontal player wins -------------
					for i in 0 to 2 loop
						if not found_move then
							 if (field_player_1(i, 0) = '1' and field_player_1(i, 1) = '1' and field_player_1(i, 2) = '0' and field_player_2(i, 2) = '0') then
								field_player_2(i, 2) <= '1';
								found_move := true;
								
							 elsif (field_player_1(i, 0) = '1' and field_player_1(i, 2) = '1' and field_player_1(i, 1) = '0' and field_player_2(i, 1) = '0') then
								field_player_2(i, 1) <= '1';
								found_move := true;
								
							 elsif (field_player_1(i, 1) = '1' and field_player_1(i, 2) = '1' and field_player_1(i, 0) = '0' and field_player_2(i, 0) = '0') then
								field_player_2(i, 0) <= '1';
								found_move := true;
							 end if;
						end if;
					end loop;
					
					-- check for vertical player wins ---------------
					for i in 0 to 2 loop
						if not found_move then
							if (field_player_1(0, i) = '1' and field_player_1(1, i) = '1' and field_player_1(2, i) = '0' and field_player_2(2, i) = '0') then
							  field_player_2(2, i) <= '1';
							  found_move := true;
							  
							elsif (field_player_1(0, i) = '1' and field_player_1(2, i) = '1' and field_player_1(1, i) = '0' and field_player_2(1, i) = '0') then
							  field_player_2(1, i) <= '1';
							  found_move := true;
							  
							elsif (field_player_1(1, i) = '1' and field_player_1(2, i) = '1' and field_player_1(0, i) = '0' and field_player_2(0, i) = '0') then
							  field_player_2(0, i) <= '1';
							  found_move := true;
							end if;
						end if;
					end loop;
					
					-- check for diagonal player wins ---------------
					if not found_move then
						if (field_player_1(0, 0) = '1' and field_player_1(1, 1) = '1' and field_player_1(2, 2) = '0' and field_player_2(2, 2) = '0') then
							 field_player_2(2, 2) <= '1';
							 found_move := true;
							 
						elsif (field_player_1(0, 0) = '1' and field_player_1(2, 2) = '1' and field_player_1(1, 1) = '0' and field_player_2(1, 1) = '0') then
							 field_player_2(1, 1) <= '1';
							 found_move := true;
							 
						elsif (field_player_1(1, 1) = '1' and field_player_1(2, 2) = '1' and field_player_1(0, 0) = '0' and field_player_2(0, 0) = '0') then
							 field_player_2(0, 0) <= '1';
							 found_move := true;
							 
						elsif (field_player_1(0, 2) = '1' and field_player_1(1, 1) = '1' and field_player_1(2, 0) = '0' and field_player_2(2, 0) = '0') then
							 field_player_2(2, 0) <= '1';
							 found_move := true;
							 
						elsif (field_player_1(0, 2) = '1' and field_player_1(2, 0) = '1' and field_player_1(1, 1) = '0' and field_player_2(1, 1) = '0') then
							 field_player_2(1, 1) <= '1';
							 found_move := true;
							 
						elsif (field_player_1(1, 1) = '1' and field_player_1(2, 0) = '1' and field_player_1(0, 2) = '0' and field_player_2(0, 2) = '0') then
							 field_player_2(0, 2) <= '1';
							 found_move := true;
						end if;
					end if;
					
					-- check for next available spot ----------------
					-- Check for any available side
					if not found_move then
						if field_player_1(0, 1) = '0' and field_player_2(0, 1) = '0' then
							field_player_2(0, 1) <= '1';
							found_move := true;
							
						elsif field_player_1(1, 0) = '0' and field_player_2(1, 0) = '0' then
							field_player_2(1, 0) <= '1';
							found_move := true;
							
						elsif field_player_1(1, 2) = '0' and field_player_2(1, 2) = '0' then
							field_player_2(1, 2) <= '1';
							found_move := true;
							
						elsif field_player_1(2, 1) = '0' and field_player_2(2, 1) = '0' then
						   field_player_2(2, 1) <= '1';
						   found_move := true;
						end if;
					end if;
					
					
					if not found_move then
						 -- Take opposite corner
						if field_player_1(0, 0) = '1' and field_player_1(2, 2) = '0' and field_player_2(2, 2) = '0' then
							field_player_2(2, 2) <= '1';
							found_move := true;
							
						elsif field_player_1(2, 2) = '1' and field_player_1(0, 0) = '0' and field_player_2(0, 0) = '0' then
							field_player_2(0, 0) <= '1';
							found_move := true;
							
						elsif field_player_1(0, 2) = '1' and field_player_1(2, 0) = '0' and field_player_2(2, 0) = '0' then
							field_player_2(2, 0) <= '1';
							found_move := true;
							
						elsif field_player_1(2, 0) = '1' and field_player_1(0, 2) = '0' and field_player_2(0, 2) = '0' then
							field_player_2(0, 2) <= '1';
							found_move := true;
						end if;
					end if;
					
					-- Check for any available corner
					if not found_move then
						if field_player_1(0, 0) = '0' and field_player_2(0, 0) = '0' then
							field_player_2(0, 0) <= '1';
							found_move := true;
							
						elsif field_player_1(0, 2) = '0' and field_player_2(0, 2) = '0' then
							field_player_2(0, 2) <= '1';
							found_move := true;
							
						elsif field_player_1(2, 0) = '0' and field_player_2(2, 0) = '0' then
							field_player_2(2, 0) <= '1';
							found_move := true;
							
						elsif field_player_1(2, 2) = '0' and field_player_2(2, 2) = '0' then
							field_player_2(2, 2) <= '1';
							found_move := true;
						end if;
					end if;

					-- check for next available spot ----------------
					if not found_move then
						for i in 0 to 2 loop
							for j in 0 to 2 loop
								if not found_move and field_player_1(i, j) = '0' and field_player_2(i, j) = '0' then
									field_player_2(i, j) <= '1';
									found_move := true;
								end if;
							end loop;
						end loop;
					end if;

					state_player_turn <= Player_1_Turn;
					
			end case;
		end if;
		
		-- check for winning combination ---------------------------------
		-- check for a draw ----------------------------------------------
		if (field_player_1(0,0) = '1' xor field_player_2(0,0) = '1') and
			(field_player_1(1,0) = '1' xor field_player_2(1,0) = '1') and
			(field_player_1(2,0) = '1' xor field_player_2(2,0) = '1') and
			(field_player_1(0,1) = '1' xor field_player_2(0,1) = '1') and
			(field_player_1(1,1) = '1' xor field_player_2(1,1) = '1') and
			(field_player_1(2,1) = '1' xor field_player_2(2,1) = '1') and
			(field_player_1(0,2) = '1' xor field_player_2(0,2) = '1') and
			(field_player_1(1,2) = '1' xor field_player_2(1,2) = '1') and
			(field_player_1(2,2) = '1' xor field_player_2(2,2) = '1') then

			winner <= 4;
		end if;
		
		-- player 1 --------------------------------------------------
		-- horizontal lines ----------------------------------------
		for i in 0 to 2 loop
			if field_player_1(i, 0) = '1' and field_player_1(i, 1) = '1' and field_player_1(i, 2) = '1' then
				winner <= 1;
			end if;
		end loop;
		
		-- vertical lines ------------------------------------------
		for i in 0 to 2 loop
			if field_player_1(0, i) = '1' and field_player_1(1, i) = '1' and field_player_1(2, i) = '1' then
				winner <= 1;
			end if;
		end loop;
		
		-- diagonal lines ------------------------------------------
		if (field_player_1(0, 0) = '1' and field_player_1(1, 1) = '1' and field_player_1(2, 2) = '1') or
			(field_player_1(0, 2) = '1' and field_player_1(1, 1) = '1' and field_player_1(2, 0) = '1') then
			winner <= 1;
		end if;
		
		-- player 2 --------------------------------------------------
		-- horizontal lines ----------------------------------------		
		for i in 0 to 2 loop
			if field_player_2(i, 0) = '1' and field_player_2(i, 1) = '1' and field_player_2(i, 2) = '1' then
				if gamemode = Game_1_Player then
					winner <= 3;
				elsif gamemode = Game_2_Player then
					winner <= 2;
				end if;
			end if;
		end loop;
		
		-- vertical lines ------------------------------------------		
		for i in 0 to 2 loop
			if field_player_2(0, i) = '1' and field_player_2(1, i) = '1' and field_player_2(2, i) = '1' then
				if gamemode = Game_1_Player then
					winner <= 3;
				elsif gamemode = Game_2_Player then
					winner <= 2;
				end if;
			end if;
		end loop;
		
		-- diagonal lines ------------------------------------------
		if (field_player_2(0, 0) = '1' and field_player_2(1, 1) = '1' and field_player_2(2, 2) = '1') or
			(field_player_2(0, 2) = '1' and field_player_2(1, 1) = '1' and field_player_2(2, 0) = '1') then
			if gamemode = Game_1_Player then
				winner <= 3;
			elsif gamemode = Game_2_Player then
				winner <= 2;
			end if;
		end if;
		
		-- Cursor --------------------------------------------------------
		-- First Row -------------------------------------------------
		if cursor_x_pos = 0 and cursor_y_pos = 0 then
			cursor_position <= A0;
		elsif cursor_x_pos = 1 and cursor_y_pos = 0 then
			cursor_position <= B0;
		elsif cursor_x_pos = 2 and cursor_y_pos = 0 then
			cursor_position <= C0;
		-- Second Row ------------------------------------------------
		elsif cursor_x_pos = 0 and cursor_y_pos = 1 then
			cursor_position <= A1;
		elsif cursor_x_pos = 1 and cursor_y_pos = 1 then
			cursor_position <= B1;
		elsif cursor_x_pos = 2 and cursor_y_pos = 1 then
			cursor_position <= C1;
		-- Third Row -------------------------------------------------
		elsif cursor_x_pos = 0 and cursor_y_pos = 2 then
			cursor_position <= A2;
		elsif cursor_x_pos = 1 and cursor_y_pos = 2 then
			cursor_position <= B2;
		elsif cursor_x_pos = 2 and cursor_y_pos = 2 then
			cursor_position <= C2;
		end if;

-- Output for graphic --------------------------------------------
		cursor_pos 	<= cursor_position;
		field_p1		<= field_player_1;
		field_p2 	<= field_player_2;
		
	end if;
	
end process;
end architecture;

-- <-- end of file