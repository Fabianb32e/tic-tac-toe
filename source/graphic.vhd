--------------------------------------------------------------------------------
--
--	File Name: graphic.vhd
--
--	VGA output: 1152 x 864 @ 60 Hz; 81.62 MHz pixel frequency
--
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- Libraries
--------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_unsigned.all;
use work.my_package.all;

--------------------------------------------------------------------------------
-- Entity
--------------------------------------------------------------------------------
entity graphic is
	generic(	vga_color_depth	: integer		:= 4);
	
	port(		clk					: in  std_logic;																-- clock
				reset					: in  std_logic;																-- Reset
				state					: in  state_graphics;														-- state to change state machine
				cursor_pos			: in  state_cursor_pos;														-- state of cursor position
				cursor_col			: in  state_cursor_color;													-- state of cursor color
				field_p1				: in  field_player;															-- player 1 fields
				field_p2				: in  field_player;															-- player 2 fields
				current_player		: in  player_turn;															-- state of which players turn
				winner				: in  integer range 0 to 4 := 0;											-- winner = 0 -> no winner, winner = 1 -> player 1, winner = 2 -> player 2, winner = 3 -> draw
				gamemode				: in  game_state;																-- gamemode to know, if 1 Player or 2 Player mode was selected
				vga_hsync			: out std_logic := '0';														-- vga horizontal sync
				vga_vsync			: out std_logic := '0';														-- vga vertical   sync
				vga_red				: out std_logic_vector(vga_color_depth - 1 downto 0) := x"0";	-- vga red   channel
				vga_green			: out std_logic_vector(vga_color_depth - 1 downto 0) := x"0";	-- vga green channel
				vga_blue				: out std_logic_vector(vga_color_depth - 1 downto 0) := x"0";	-- vga blue  channel
				allow_update		: out std_logic := '0');													-- flag to allow game updates
				
end graphic;

--------------------------------------------------------------------------------
-- Architecture
--------------------------------------------------------------------------------
architecture Beh_Graphic of graphic is
	
	-- VGA Sync component ---------------------------------------------------
	component vga_sync is
		generic(	hor_front_porch 	: integer  	:= 	64;									-- horizontal front porch
					hor_sync_pulse  	: integer	:=	  120;									-- horizontal sync pulse
					hor_back_porch		: integer	:=	  184;									-- horizontal back porch
					hor_polarity		: std_logic :=   '0';									-- horizontal polarity
					hor_tot_pixel		: integer	:=  1520;									-- horizontal total of pixels
					
					ver_front_porch 	: integer	:=     1;									-- vertical front porch
					ver_sync_pulse		: integer	:=		 3;									-- vertical sync pulse
					ver_back_porch		: integer	:=		27;									-- vertical back porch
					ver_polarity		: std_logic :=	  '1';									-- vertical polarity
					ver_tot_pixel		: integer	:=  895);									-- vertical total of pixels
			
		port(		vga_clock			: in	std_logic;
					reset					: in 	std_logic;
					hsync					: out	std_logic;
					vsync					: out std_logic;
					xpos					: out integer range 0 to hor_tot_pixel;		-- x position of the vga image area
					ypos 					: out integer range 0 to ver_tot_pixel;		-- y position of the vga image area
					video_on				: out std_logic);										-- alow to send color data to VGA
	end component vga_sync;
	
	-- TicTacToe Title Icon component ----------------------------------------
	component Title_Icon is
		port(		address				: IN  STD_LOGIC_VECTOR (10 DOWNTO 0);
					clock					: IN  STD_LOGIC  := '1';
					q						: OUT STD_LOGIC_VECTOR (11 DOWNTO 0));
	end component Title_Icon;
	
	-- Player 1 Icon component -----------------------------------------------
	component Player_1_Icon is
		port(		address				: IN  STD_LOGIC_VECTOR (10 DOWNTO 0);
					clock					: IN  STD_LOGIC := '1';
					q						: OUT STD_LOGIC_VECTOR (11 DOWNTO 0));
	end component Player_1_Icon;
	
	-- Player 2 Icon component -----------------------------------------------
	component Player_2_Icon is
		port(		address				: IN  STD_LOGIC_VECTOR (10 DOWNTO 0);
					clock					: IN  STD_LOGIC := '1';
					q						: OUT STD_LOGIC_VECTOR (11 DOWNTO 0));
	end component Player_2_Icon;
	
	-- PC Icon component -----------------------------------------------------
	component PC_Icon is
		port(		address				: IN  STD_LOGIC_VECTOR (10 DOWNTO 0);
					clock					: IN  STD_LOGIC := '1';
					q						: OUT STD_LOGIC_VECTOR (11 DOWNTO 0));
	end component PC_Icon;
	
	-- Draw Game Icon --------------------------------------------------------
	component Draw_Game_Icon is
		port(		address				: IN  STD_LOGIC_VECTOR (11 DOWNTO 0);
					clock					: IN  STD_LOGIC  := '1';
					q						: OUT STD_LOGIC_VECTOR (11 DOWNTO 0));
	end component Draw_Game_Icon;
	
	-- Winner Trophy ---------------------------------------------------------
	component Winner_Trophy is
		port(		address				: IN  STD_LOGIC_VECTOR (12 DOWNTO 0);
					clock					: IN  STD_LOGIC := '1';
					q						: OUT STD_LOGIC_VECTOR (11 DOWNTO 0));
	end component Winner_Trophy;
	
	-- Numpad Button 1 -------------------------------------------------------
	component Numpad_Button_1 is
		port(		address				: IN  STD_LOGIC_VECTOR (11 DOWNTO 0);
					clock					: IN  STD_LOGIC := '1';
					q						: OUT STD_LOGIC_VECTOR (0 DOWNTO 0));
	end component Numpad_Button_1;
	
	-- Numpad Button 2 -------------------------------------------------------
	component Numpad_Button_2 is
		port(		address				: IN  STD_LOGIC_VECTOR (11 DOWNTO 0);
					clock					: IN  STD_LOGIC := '1';
					q						: OUT STD_LOGIC_VECTOR (0 DOWNTO 0));
	end component Numpad_Button_2;
	
	-- Enter Button ----------------------------------------------------------
	component Enter_Button is
		port(		address				: IN  STD_LOGIC_VECTOR (11 DOWNTO 0);
					clock					: IN  STD_LOGIC  := '1';
					q						: OUT STD_LOGIC_VECTOR (0 DOWNTO 0));
	end component Enter_Button;
		
	
	-- Signals and Constants -------------------------------------------------
	constant const_hor_tot_pixel			: integer := 1520;																-- horizontal total of pixels
	constant const_ver_tot_pixel			: integer :=  895;																-- vertical   total of pixels
	
	constant const_hor_pixel				: integer := 1152;																-- horizontal pixels shown
	constant const_ver_pixel				: integer :=  864;																-- vertical   pixels shown
	
	constant const_hor_grid_space			: integer := 180;																	-- horizontal grid space
	constant const_ver_grid_space			: integer := 180;																	-- vertical   grid space

	constant const_hor_grid_space_2		: integer := const_hor_grid_space / 2;										-- 1/2 horizontal grid space
	constant const_ver_grid_space_2		: integer := const_ver_grid_space / 2;										-- 1/2 vertical   grid space
	
	constant const_hor_grid_offset		: integer := 307;																	-- horizontal grid offset
	constant const_ver_grid_offset		: integer := 179;																	-- vertical   grid offset
	
	constant circle_thikness				: integer := 20;
	
	constant circle_radius_max				: integer := (const_hor_grid_space / 2 - 20);
	constant circle_radius_max_2			: integer := circle_radius_max * circle_radius_max;
	
	constant circle_radius_min				: integer := circle_radius_max - circle_thikness;
	constant circle_radius_min_2			: integer := circle_radius_min * circle_radius_min;
	
	-- Current Player Icon ------------------------------------
	constant pic_current_player_x			: integer := 44;																	-- Current Player Icon x Size
	constant pic_current_player_y			: integer := 26;																	-- Current Player Icon y Size
	
	constant player_icon_scale_factor	: integer := 4;																	-- Current Player Scale Factor
	
	constant current_player_x_offset		: integer := 20;																	-- Current Player x Offset
	constant current_player_y_offset		: integer := const_ver_grid_offset;											-- Current Player y Offset
	
	constant current_player_x_size		: integer := player_icon_scale_factor * pic_current_player_x;		-- Current Player x endsize
	constant current_player_y_size		: integer := player_icon_scale_factor * pic_current_player_y;		-- Current Player y endsize
	
	-- Trophy -------------------------------------------------
	constant pic_trophy_x					: integer := 70;
	constant pic_trophy_y					: integer := 97;
	
	constant trophy_scale_factor			: integer := 5;
	
	constant trophy_x_size					: integer := pic_trophy_x * (trophy_scale_factor + 1);
	constant trophy_y_size					: integer := pic_trophy_y * (trophy_scale_factor + 1);
	
	constant trophy_x_offset				: integer := (const_hor_tot_pixel / 3) - (trophy_x_size / 2);
	constant trophy_y_offset				: integer := (const_ver_tot_pixel / 2) - (trophy_y_size / 2);
	
	signal 	trophy_scale_counter_x		: integer range 0 to const_hor_tot_pixel;
	signal 	trophy_scale_counter_y		: integer range 0 to const_ver_tot_pixel;
	
	signal 	trophy_offset_y				: std_logic_vector(12 downto 0) := (others => '0');
	
	-- Winner Icon --------------------------------------------
	constant	const_winner_offset_x		: integer := (const_hor_tot_pixel / 2) + (current_player_x_size / 4);
	constant	const_winner_offset_y		: integer := (const_ver_tot_pixel / 3) + (current_player_y_size / 1);
	
	-- Title Icon ---------------------------------------------
	constant	title_icon_x					: integer := 86;
	constant	title_icon_y					: integer := 14;
	
	constant title_icon_scale_factor		: integer :=  7;
	
	constant title_icon_x_size				: integer := title_icon_x * (title_icon_scale_factor + 1);
	constant title_icon_y_size				: integer := title_icon_y * (title_icon_scale_factor + 1);
	
	constant title_icon_x_offset			: integer := (const_hor_tot_pixel / 3) - (title_icon_x_size / 2) + 70;
	constant title_icon_y_offset			: integer := (const_ver_tot_pixel / 2) - (title_icon_y_size / 2) - 20;
	
	signal 	title_icon_counter_x			: integer range 0 to const_hor_tot_pixel;
	signal 	title_icon_counter_y			: integer range 0 to const_ver_tot_pixel;
	
	signal 	title_icon_offset_y			: std_logic_vector(10 downto 0) := (others => '0');
	
	-- Enter Button Icon --------------------------------------
	constant	enter_icon_x					: integer := 56;
	constant	enter_icon_y					: integer := 64;
	
	constant enter_icon_scale_factor		: integer :=  1;
	
	constant enter_icon_x_size				: integer := enter_icon_x * (enter_icon_scale_factor + 1);
	constant enter_icon_y_size				: integer := enter_icon_y * (enter_icon_scale_factor + 1);
	
	constant enter_icon_x_offset			: integer := const_hor_tot_pixel - enter_icon_x_size - 550;
	constant enter_icon_y_offset			: integer := const_ver_tot_pixel - enter_icon_y_size - 100;
	
	signal 	enter_icon_counter_x			: integer range 0 to const_hor_tot_pixel;
	signal 	enter_icon_counter_y			: integer range 0 to const_ver_tot_pixel;
	
	signal 	enter_icon_offset_y			: std_logic_vector(11 downto 0) := (others => '0');
	
	-- Draw Game Icon -----------------------------------------
	constant draw_game_icon_x					: integer := 82;
	constant draw_game_icon_y					: integer := 25;
	
	constant draw_game_icon_scale_factor	: integer :=  5;
	
	constant draw_game_icon_x_size			: integer := draw_game_icon_x * (draw_game_icon_scale_factor + 1);
	constant draw_game_icon_y_size			: integer := draw_game_icon_y * (draw_game_icon_scale_factor + 1);
	
	constant draw_game_icon_x_offset			: integer := (const_hor_tot_pixel / 3) - (draw_game_icon_x_size / 2) + 60;
	constant draw_game_icon_y_offset			: integer := (const_ver_tot_pixel / 2) - (draw_game_icon_y_size / 2) - 20;
	
	signal 	draw_game_icon_counter_x		: integer range 0 to const_hor_tot_pixel;
	signal 	draw_game_icon_counter_y		: integer range 0 to const_ver_tot_pixel;
	
	signal 	draw_game_icon_offset_y			: std_logic_vector(11 downto 0) := (others => '0');
	
	-- Numpad Button 1 & 2 icon -------------------------------
	constant numpad_button_icon_x					: integer := 50;
	constant numpad_button_icon_y					: integer := 49;
	
	constant numpad_button_icon_scale_factor	: integer := 1;
	
	constant numpad_button_icon_x_size			: integer := numpad_button_icon_x * (numpad_button_icon_scale_factor + 1);
	constant numpad_button_icon_y_size			: integer := numpad_button_icon_y * (numpad_button_icon_scale_factor + 1);
	
	constant numpad_button_1_x_offset			: integer := const_hor_tot_pixel - (const_hor_tot_pixel / 2) - 350;
	constant numpad_button_1_y_offset			: integer := const_ver_tot_pixel - numpad_button_icon_y_size - 100;
	
	constant numpad_button_2_x_offset			: integer := const_hor_tot_pixel - (const_hor_tot_pixel / 2) - 100;
	constant numpad_button_2_y_offset			: integer := const_ver_tot_pixel - numpad_button_icon_y_size - 100;
	
	signal 	numpad_button_1_counter_x			: integer range 0 to const_hor_tot_pixel;
	signal 	numpad_button_1_counter_y			: integer range 0 to const_ver_tot_pixel;
	
	signal 	numpad_button_2_counter_x			: integer range 0 to const_hor_tot_pixel;
	signal 	numpad_button_2_counter_y			: integer range 0 to const_ver_tot_pixel;
	
	signal 	numpad_button_1_offset_y			: std_logic_vector(11 downto 0) := (others => '0');
	signal 	numpad_button_2_offset_y			: std_logic_vector(11 downto 0) := (others => '0');
	
	-- Type Declarations for Arrays -----------------------------------------
	type 		int_array_3 					is array (0 to 2) of integer range -const_hor_tot_pixel to const_hor_tot_pixel;
	type 		int_array_3_unsigned 		is array (0 to 2) of integer range 0 to const_hor_tot_pixel * const_hor_tot_pixel;
	
	-- Signals --------------------------------------------------------------
	signal 	title_icon_adr					: std_logic_vector(10 downto 0)	:= (others => '0');	-- title icon address
	signal 	current_player_icon1_adr	: std_logic_vector(10 downto 0) 	:= (others => '0');	-- current player icon 1 address
	signal 	current_player_icon2_adr	: std_logic_vector(10 downto 0) 	:= (others => '0');	-- current player icon 2 address
	signal 	pc_icon_adr						: std_logic_vector(10 downto 0) 	:= (others => '0');	-- pc icon address
	signal 	draw_game_icon_adr			: std_logic_vector(11 downto 0)	:= (others => '0');	-- draw game icon adress
	signal 	winner_trophy_adr				: std_logic_vector(12 downto 0) 	:= (others => '0');	-- winner trophy address
	signal 	numpad_button_1_adr			: std_logic_vector(11 downto 0) 	:= (others => '0');	-- numpad button 1 address
	signal 	numpad_button_2_adr			: std_logic_vector(11 downto 0) 	:= (others => '0');	-- numpad button 2 address
	signal 	enter_button_adr				: std_logic_vector(11 downto 0)	:= (others => '0');	-- enter button address
	
	signal 	title_icon_dat					: std_logic_vector(11 downto 0)	:= (others => '0');	-- title icon data
	signal 	current_player_1_icon_dat	: std_logic_vector(11 downto 0) 	:= (others => '0');	-- current player 1 icon data
	signal 	current_player_2_icon_dat	: std_logic_vector(11 downto 0) 	:= (others => '0');	-- current player 2 icon data
	signal 	pc_icon_dat						: std_logic_vector(11 downto 0) 	:= (others => '0');	-- pc icon data
	signal 	draw_game_icon_dat			: std_logic_vector(11 downto 0)	:= (others => '0');	-- draw game icon data
	signal 	winner_trophy_dat				: std_logic_vector(11 downto 0) 	:= (others => '0');	-- winner trophy data
	signal 	numpad_button_1_dat			: std_logic_vector( 0 downto 0) 	:= (others => '0');	-- numpad button 1 data
	signal 	numpad_button_2_dat			: std_logic_vector( 0 downto 0) 	:= (others => '0');	-- numpad button 2 data
	signal 	enter_button_dat				: std_logic_vector( 0 downto 0) 	:= (others => '0');	-- enter button data
	
	signal 	circle_center_x				: int_array_3 := (others => 0);
	signal 	circle_center_y				: int_array_3 := (others => 0);
	
	signal 	vga_xpos							: integer range 0 to const_hor_tot_pixel := 0;
	signal 	vga_ypos							: integer range 0 to const_ver_tot_pixel := 0;
	signal 	vga_video_on					: std_logic;
	
	signal 	delta_x							: int_array_3;
	signal 	delta_x_2						: int_array_3_unsigned;
	
	signal 	delta_y							: int_array_3;
	signal 	delta_y_2						: int_array_3_unsigned;	

	-- Additional Signals for Player Icon Scaling ---------------------------
	signal 	player_icon_x					: integer range 0 to pic_current_player_x - 1 := 0;
	signal 	player_icon_y					: integer range 0 to pic_current_player_y - 1 := 0;

begin

	-- VGA Sync -------------------------------
	my_vga_sync : vga_sync
	generic map(hor_tot_pixel 	=> const_hor_tot_pixel,
					ver_tot_pixel	=> const_ver_tot_pixel)
	
	port map(	vga_clock		=> clk,
					reset				=> reset,
					hsync				=> vga_hsync,
					vsync				=> vga_vsync,
					xpos 				=> vga_xpos,
					ypos 				=> vga_ypos,
					video_on			=> vga_video_on);
					
	-- Title Icon -----------------------------
	my_title_icon : Title_Icon
	port map(	address			=> title_icon_adr,
					clock				=> clk,
					q					=> title_icon_dat);
					
	-- Player 1 Icon --------------------------
	my_player_1_icon : Player_1_Icon
	port map(	address			=> current_player_icon1_adr,
					clock				=> clk,
					q					=> current_player_1_icon_dat);
	
	-- Player 2 Icon --------------------------
	my_player_2_icon : Player_2_Icon
	port map(	address			=> current_player_icon2_adr,
					clock				=> clk,
					q					=> current_player_2_icon_dat);
					
	-- PC Icon --------------------------------
	my_pc_icon : PC_Icon
	port map(	address			=> pc_icon_adr,
					clock				=> clk,
					q					=> pc_icon_dat);
					
	-- Draw Game Icon -------------------------
	my_draw_game_icon : Draw_Game_Icon
	port map(	address			=> draw_game_icon_adr,
					clock				=> clk,
					q					=> draw_game_icon_dat);
	
	-- Winner Trophy --------------------------
	my_winner_trophy : Winner_Trophy
	port map(	address			=> winner_trophy_adr,
					clock				=> clk,
					q					=> winner_trophy_dat);
					
	-- Numpad Button 1 ------------------------
	my_numpad_button_1 : Numpad_Button_1
	port map(	address			=> numpad_button_1_adr,
					clock				=> clk,
					q					=> numpad_button_1_dat);
	
	-- Numpad Button 2 ------------------------
	my_numpad_button_2 : Numpad_Button_2
	port map(	address			=> numpad_button_2_adr,
					clock				=> clk,
					q					=> numpad_button_2_dat);
	
	-- Enter Button ---------------------------
	my_enter_button : Enter_Button
	port map(	address			=> enter_button_adr,
					clock				=> clk,
					q					=> enter_button_dat);
	
-- Graphic process -------------------------------------------------------
Graphic:
process(all)
begin
	
	if rising_edge(clk) then

		-- Reset ROM Address ---------------------------------------------
		if vga_xpos = 0 and vga_ypos = 0 then
			title_icon_adr					<= (others => '0');
			current_player_icon1_adr 	<= (others => '0');
			current_player_icon2_adr 	<= (others => '0');
			pc_icon_adr						<= (others => '0');
			winner_trophy_adr				<= (others => '0');
			numpad_button_1_adr			<= (others => '0');
			numpad_button_2_adr			<= (others => '0');
		end if; 
		
		-- Update for Graphics -------------------------------------------
		allow_update <= not vga_video_on;
	
		if vga_video_on = '0' then
			vga_red 		<= x"0";
			vga_green	<= x"0";
			vga_blue		<= x"0";
		end if;
	
		case state is
			-- Start Menu -------------------------------------------------
			when Start_Menu =>
				if vga_video_on = '1' then
				
					-- set vga rgb channels to 0 ------------------------
					vga_red 		<= x"0";
					vga_green	<= x"0";
					vga_blue		<= x"0";
				
					-- draw Start Menu ----------------------------------
					
					-- Display TicTacToe Logo -----------------------
					-- Check if the current pixel is within the title icon's horizontal bounds
					if vga_xpos >= title_icon_x_offset and vga_xpos < title_icon_x_offset + title_icon_x_size then
						-- Check if the current pixel is within the title icon's vertical bounds
						if vga_ypos >= title_icon_y_offset and vga_ypos < title_icon_y_offset + title_icon_y_size then
							-- Calculate scaled positions
							title_icon_counter_x <= title_icon_counter_x + 1;

							-- Update the title icon address every 'title_icon_scale_factor' pixels
							if title_icon_counter_x = title_icon_scale_factor then
								title_icon_adr <= title_icon_adr + 1;
								title_icon_counter_x <= 0;
							end if;

							-- Check for the end of the row
							if vga_xpos = title_icon_x_offset + title_icon_x_size - 1 then
								title_icon_counter_y <= title_icon_counter_y + 1;
								title_icon_counter_x <= 0;

								-- Update the offset every 'title_icon_scale_factor' rows
								if title_icon_counter_y = title_icon_scale_factor then
									title_icon_counter_y <= 0;
									title_icon_offset_y  <= title_icon_offset_y + title_icon_x;
								end if;
							end if;

							-- Set VGA output colors from the title icon data
							vga_red   <= title_icon_dat(11 downto 8);
							vga_green <= title_icon_dat( 7 downto 4);
							vga_blue  <= title_icon_dat( 3 downto 0);

						else
							-- Reset vertical scaling and offset if outside title icon vertical bounds
							title_icon_counter_y <= 0;
							title_icon_offset_y  <= (others => '0');
						end if;
					else
						-- Reset the title icon address offset if outside title icon horizontal bounds
						title_icon_adr <= title_icon_offset_y;
					end if;
					
					-- Numpad Button 1 ------------------------------
					-- Check if the current pixel is within the title icon's horizontal bounds
					if vga_xpos >= numpad_button_1_x_offset and vga_xpos < numpad_button_1_x_offset + numpad_button_icon_x_size then
						-- Check if the current pixel is within the title icon's vertical bounds
						if vga_ypos >= numpad_button_1_y_offset and vga_ypos < numpad_button_1_y_offset + numpad_button_icon_y_size then
							-- Calculate scaled positions
							numpad_button_1_counter_x <= numpad_button_1_counter_x + 1;

							-- Update the title icon address every 'title_icon_scale_factor' pixels
							if numpad_button_1_counter_x = numpad_button_icon_scale_factor then
								numpad_button_1_adr 				<= numpad_button_1_adr + 1;
								numpad_button_1_counter_x 		<= 0;
							end if;

							-- Check for the end of the row
							if vga_xpos = numpad_button_1_x_offset + numpad_button_icon_x_size - 1 then
								numpad_button_1_counter_y 		<= numpad_button_1_counter_y + 1;
								numpad_button_1_counter_x 		<= 0;

								-- Update the offset every 'title_icon_scale_factor' rows
								if numpad_button_1_counter_y = numpad_button_icon_scale_factor then
									numpad_button_1_counter_y	<= 0;
									numpad_button_1_offset_y  	<= numpad_button_1_offset_y + numpad_button_icon_x;
								end if;
							end if;

							-- Set VGA output colors from the title icon data
							if numpad_button_1_dat(0 downto 0) = x"1" then
								vga_red 		<= x"f";
								vga_green	<= x"f";
								vga_blue		<= x"f";
							else
								vga_red 		<= x"0";
								vga_green	<= x"0";
								vga_blue		<= x"0";
							end if;

						else
							-- Reset vertical scaling and offset if outside title icon vertical bounds
							numpad_button_1_counter_y <= 0;
							numpad_button_1_offset_y  <= (others => '0');
						end if;
					else
						-- Reset the title icon address offset if outside title icon horizontal bounds
						numpad_button_1_adr <= numpad_button_1_offset_y;
					end if;
					
					-- Numpad Button 2 ------------------------------
					-- Check if the current pixel is within the title icon's horizontal bounds
					if vga_xpos >= numpad_button_2_x_offset and vga_xpos < numpad_button_2_x_offset + numpad_button_icon_x_size then
						-- Check if the current pixel is within the title icon's vertical bounds
						if vga_ypos >= numpad_button_2_y_offset and vga_ypos < numpad_button_2_y_offset + numpad_button_icon_y_size then
							-- Calculate scaled positions
							numpad_button_2_counter_x <= numpad_button_2_counter_x + 1;

							-- Update the title icon address every 'title_icon_scale_factor' pixels
							if numpad_button_2_counter_x = numpad_button_icon_scale_factor then
								numpad_button_2_adr 				<= numpad_button_2_adr + 1;
								numpad_button_2_counter_x 		<= 0;
							end if;

							-- Check for the end of the row
							if vga_xpos = numpad_button_2_x_offset + numpad_button_icon_x_size - 1 then
								numpad_button_2_counter_y 		<= numpad_button_2_counter_y + 1;
								numpad_button_2_counter_x 		<= 0;

								-- Update the offset every 'title_icon_scale_factor' rows
								if numpad_button_2_counter_y = numpad_button_icon_scale_factor then
									numpad_button_2_counter_y	<= 0;
									numpad_button_2_offset_y  	<= numpad_button_2_offset_y + numpad_button_icon_x;
								end if;
							end if;

							-- Set VGA output colors from the title icon data
							if numpad_button_2_dat(0 downto 0) = x"1" then
								vga_red 		<= x"f";
								vga_green	<= x"f";
								vga_blue		<= x"f";
							else
								vga_red 		<= x"0";
								vga_green	<= x"0";
								vga_blue		<= x"0";
							end if;

						else
							-- Reset vertical scaling and offset if outside title icon vertical bounds
							numpad_button_2_counter_y <= 0;
							numpad_button_2_offset_y  <= (others => '0');
						end if;
					else
						-- Reset the title icon address offset if outside title icon horizontal bounds
						numpad_button_2_adr <= numpad_button_2_offset_y;
					end if;
				
				end if;
			
			-- Game -------------------------------------------------------
			when Game =>
				if vga_video_on = '1' then
					
					-- set vga rgb channels to 0 ------------------------
					vga_red 	<= x"0";
					vga_green	<= x"0";
					vga_blue	<= x"0";
							
					-- draw grid ----------------------------------------
					-- horizontal lines -----------------------------
					for i in 0 to 3 loop
						if vga_ypos >= const_ver_grid_offset + (i*const_ver_grid_space) -5 and vga_ypos <= const_ver_grid_offset + (i*const_ver_grid_space) +5 
							and vga_xpos >= const_hor_grid_offset -5 and vga_xpos <= const_hor_grid_offset + (3 * const_hor_grid_space) +5 then
							vga_red 		<= x"f";
							vga_green	<= x"f";
							vga_blue		<= x"f";
						end if;
					end loop;
					
					-- vertical lines -------------------------------
					for i in 0 to 3 loop
						if vga_xpos >= const_hor_grid_offset + (i*const_hor_grid_space) - 5 and vga_xpos <= const_hor_grid_offset + (i*const_hor_grid_space) + 5 and 
							vga_ypos >= const_ver_grid_offset - 5 and vga_ypos <= const_ver_grid_offset + (3 * const_ver_grid_space) + 5 then
							vga_red   	<= x"f";
							vga_green 	<= x"f";
							vga_blue  	<= x"f";
						end if;
					end loop;
					
					-- Cursor -------------------------------------
					for i in 0 to 2 loop
						for j in 0 to 2 loop
							if cursor_pos = state_cursor_pos'val(3*i + j) then
								if vga_xpos >= const_hor_grid_offset + (i*const_hor_grid_space) +7 and vga_xpos <= const_hor_grid_offset + (i*const_hor_grid_space) +11 and vga_ypos >= const_ver_grid_offset + (j*const_ver_grid_space) +7 and vga_ypos <= const_ver_grid_offset + ((j+1)*const_ver_grid_space) -7 then
									if cursor_col = red then
										vga_red 		<= x"f";
										vga_green	<= x"0";
										vga_blue		<= x"0";
									elsif cursor_col = green then
										vga_red 		<= x"0";
										vga_green	<= x"f";
										vga_blue		<= x"0";
									end if;
								end if;
								if vga_xpos >= const_hor_grid_offset + ((i+1)*const_hor_grid_space) -11 and vga_xpos <= const_hor_grid_offset + ((i+1)*const_hor_grid_space) -7 and vga_ypos >= const_ver_grid_offset + (j*const_ver_grid_space) +7 and vga_ypos <= const_ver_grid_offset + ((j+1)*const_ver_grid_space) -7 then
									if cursor_col = red then
										vga_red 		<= x"f";
										vga_green	<= x"0";
										vga_blue		<= x"0";
									elsif cursor_col = green then
										vga_red 		<= x"0";
										vga_green	<= x"f";
										vga_blue		<= x"0";
									end if;
								end if;
								if vga_ypos >= const_ver_grid_offset + (j*const_ver_grid_space) +7 and vga_ypos <= const_ver_grid_offset + (j*const_ver_grid_space) +11 and vga_xpos >= const_hor_grid_offset + (i*const_hor_grid_space) +7 and vga_xpos <= const_hor_grid_offset + ((i+1)*const_hor_grid_space) -11 then
									if cursor_col = red then
										vga_red 		<= x"f";
										vga_green	<= x"0";
										vga_blue		<= x"0";
									elsif cursor_col = green then
										vga_red 		<= x"0";
										vga_green	<= x"f";
										vga_blue		<= x"0";
									end if;
								end if;
								if vga_ypos >= const_ver_grid_offset + ((j+1)*const_ver_grid_space) -11 and vga_ypos <= const_ver_grid_offset + ((j+1)*const_ver_grid_space) -7 and vga_xpos >= const_hor_grid_offset + (i*const_hor_grid_space) +7 and vga_xpos <= const_hor_grid_offset + ((i+1)*const_hor_grid_space) -11 then
									if cursor_col = red then
										vga_red 		<= x"f";
										vga_green	<= x"0";
										vga_blue		<= x"0";
									elsif cursor_col = green then
										vga_red 		<= x"0";
										vga_green	<= x"f";
										vga_blue		<= x"0";
									end if;
								end if;
							end if;
						end loop;
					end loop;
					
					-- Draw Symbols -------------------------------
					
					-- Calculate X Sector ---------------------
					for i in 0 to 2 loop
						if vga_xpos >= const_hor_grid_offset + (i*const_hor_grid_space) and vga_xpos <= const_hor_grid_offset + ((i+1)*const_hor_grid_space) then
							circle_center_x(i) <= const_hor_grid_offset + (i*const_hor_grid_space) + const_hor_grid_space_2;
						end if;
					end loop;
					
					-- Calculate Y Sector ---------------------
					for j in 0 to 2 loop
						if vga_ypos >= const_ver_grid_offset + (j*const_ver_grid_space) and vga_ypos <= const_ver_grid_offset + ((j+1)*const_ver_grid_space) then
							circle_center_y(j) <= const_ver_grid_offset + (j*const_ver_grid_space) + const_ver_grid_space_2;
						end if;
					end loop;
					
					-- Calculate delta value for circle ------------------
					for i in 0 to 2 loop
						delta_x(i) 		<= vga_xpos - circle_center_x(i);
						delta_x_2(i) 	<= delta_x(i) * delta_x(i);
					end loop;
					
					-- Draw Circle ----------------------------

					for j in 0 to 2 loop
						delta_y(j) 		<= vga_ypos - circle_center_y(j);
						delta_y_2(j) 	<= delta_y(j) * delta_y(j);
					end loop;

					for i in 0 to 2 loop
						for j in 0 to 2 loop
							if field_p1(i,j) = '1' and (delta_x_2(i) + delta_y_2(j) <= circle_radius_max_2) and (delta_x_2(i) + delta_y_2(j) >= circle_radius_min_2) then
								vga_red 		<= x"f";
								vga_green	<= x"0";
								vga_blue		<= x"0";
							end if;
						end loop;
					end loop;
					
					-- Draw Cross -----------------------------
					for i in 0 to 2 loop
						for j in 0 to 2 loop
							if field_p2(i,j) = '1' then
								if ((-circle_thikness < (delta_x(i) - delta_y(j)) and (delta_x(i) - delta_y(j)) < circle_thikness) and
									(-circle_radius_max <= delta_x(i) and delta_x(i) <= circle_radius_max) and
									(-circle_radius_max <= delta_y(j) and delta_y(j) <= circle_radius_max)) or 
								   ((-circle_thikness < (delta_x(i) + delta_y(j)) and (delta_x(i) + delta_y(j)) < circle_thikness) and
									(-circle_radius_max <= delta_x(i) and delta_x(i) <= circle_radius_max) and
									(-circle_radius_max <= delta_y(j) and delta_y(j) <= circle_radius_max)) then
									vga_red 		<= x"0";
									vga_green	<= x"0";
									vga_blue		<= x"f";
								end if;
							end if;
						end loop;
					end loop;
					
					-- Player Icon -----------------------------------
					if current_player = Player_1_Turn then
					
						if vga_xpos >= current_player_x_offset and vga_xpos < current_player_x_offset + current_player_x_size and 
							vga_ypos >= current_player_y_offset and vga_ypos < current_player_y_offset + current_player_y_size then
							
							-- Calculate scaled positions
							player_icon_x <= (vga_xpos - current_player_x_offset) / player_icon_scale_factor;
							player_icon_y <= (vga_ypos - current_player_y_offset) / player_icon_scale_factor;
							
							current_player_icon1_adr <= std_logic_vector(to_unsigned(player_icon_y * pic_current_player_x + player_icon_x, current_player_icon1_adr'length));
							
							vga_red 		<= current_player_1_icon_dat(11 downto 8);
							vga_green	<= current_player_1_icon_dat( 7 downto 4);
							vga_blue		<= current_player_1_icon_dat( 3 downto 0);
							
						end if;
					
					elsif current_player = Player_2_Turn then
					
						if vga_xpos >= current_player_x_offset and vga_xpos < current_player_x_offset + current_player_x_size and 
							vga_ypos >= current_player_y_offset and vga_ypos < current_player_y_offset + current_player_y_size then
							
							-- Calculate scaled positions
							player_icon_x <= (vga_xpos - current_player_x_offset) / player_icon_scale_factor;
							player_icon_y <= (vga_ypos - current_player_y_offset) / player_icon_scale_factor;
							
							current_player_icon2_adr <= std_logic_vector(to_unsigned(player_icon_y * pic_current_player_x + player_icon_x, current_player_icon2_adr'length));
							
							vga_red 		<= current_player_2_icon_dat(11 downto 8);
							vga_green	<= current_player_2_icon_dat( 7 downto 4);
							vga_blue		<= current_player_2_icon_dat( 3 downto 0);
							
						end if;
					
					elsif current_player = PC_Turn then
						-- Will not be implemented, because it is so fast, that it will not be visible on the screen
					end if;
				
				else 
					-- Calculate delta value for circle ------------------
					for j in 0 to 2 loop
						delta_y(j) <= vga_ypos - circle_center_y(j);
						delta_y_2(j) <= delta_y(j) * delta_y(j);
					end loop;
				end if; -- vga_video_on = '1'
			
			-- Winner Screen ----------------------------------------------
			when Winner_Screen =>
				if vga_video_on = '1' then
					
					vga_red 		<= x"0";
					vga_green	<= x"0";
					vga_blue		<= x"0";
					
					-- Winner Trophy ------------------------------------
					if winner = 1 or winner = 2 or winner = 3 then
						
						-- Check if the current pixel is within the trophy's horizontal bounds
						if vga_xpos >= trophy_x_offset and vga_xpos < trophy_x_offset + trophy_x_size then 
							 -- Check if the current pixel is within the trophy's vertical bounds
							 if vga_ypos >= trophy_y_offset and vga_ypos < trophy_y_offset + trophy_y_size then
								  -- Calculate scaled positions
								  trophy_scale_counter_x <= trophy_scale_counter_x + 1;
								  
								  -- Update the trophy address every 'trophy_scale_factor' pixels
								  if trophy_scale_counter_x = trophy_scale_factor then
										winner_trophy_adr <= winner_trophy_adr + 1;
										trophy_scale_counter_x <= 0;
								  end if;
								  
									-- Check for the end of the row
								  if vga_xpos = trophy_x_offset + trophy_x_size - 1 then
										trophy_scale_counter_y <= trophy_scale_counter_y + 1;
										trophy_scale_counter_x <= 0;

										-- Update the offset every 'trophy_scale_factor' rows
										if trophy_scale_counter_y = trophy_scale_factor then
											 trophy_scale_counter_y <= 0;
											 trophy_offset_y <= trophy_offset_y + pic_trophy_x;
										end if;
								  end if;
								  
								  -- Set VGA output colors from the trophy data
								  vga_red   <= winner_trophy_dat(11 downto 8);
								  vga_green <= winner_trophy_dat( 7 downto 4);
								  vga_blue  <= winner_trophy_dat( 3 downto 0);
								  
							 else
								  -- Reset vertical scaling and offset if outside trophy vertical bounds
								  trophy_scale_counter_y <= 0;
								  trophy_offset_y <= (others => '0');
							 end if;
						else
							 -- Reset the trophy address offset if outside trophy horizontal bounds
							 winner_trophy_adr <= trophy_offset_y;
						end if;
						
					end if;
					
					-- Display Enter Button --------------------------------
					-- Check if the current pixel is within the title icon's horizontal bounds
					if vga_xpos >= enter_icon_x_offset and vga_xpos < enter_icon_x_offset + enter_icon_x_size then
						-- Check if the current pixel is within the title icon's vertical bounds
						if vga_ypos >= enter_icon_y_offset and vga_ypos < enter_icon_y_offset + enter_icon_y_size then
							-- Calculate scaled positions
							enter_icon_counter_x <= enter_icon_counter_x + 1;

							-- Update the title icon address every 'title_icon_scale_factor' pixels
							if enter_icon_counter_x = enter_icon_scale_factor then
								enter_button_adr 			<= enter_button_adr + 1;
								enter_icon_counter_x 	<= 0;
							end if;

							-- Check for the end of the row
							if vga_xpos = enter_icon_x_offset + enter_icon_x_size - 1 then
								enter_icon_counter_y 	<= enter_icon_counter_y + 1;
								enter_icon_counter_x 	<= 0;

								-- Update the offset every 'title_icon_scale_factor' rows
								if enter_icon_counter_y = enter_icon_scale_factor then
									enter_icon_counter_y <= 0;
									enter_icon_offset_y  <= enter_icon_offset_y + enter_icon_x;
								end if;
							end if;

							-- Set VGA output colors from the title icon data
							if enter_button_dat(0 downto 0) = x"1" then
								vga_red 		<= x"f";
								vga_green	<= x"f";
								vga_blue		<= x"f";
							else
								vga_red 		<= x"0";
								vga_green	<= x"0";
								vga_blue		<= x"0";
							end if;

						else
							-- Reset vertical scaling and offset if outside title icon vertical bounds
							enter_icon_counter_y <= 0;
							enter_icon_offset_y  <= (others => '0');
						end if;
					else
						-- Reset the title icon address offset if outside title icon horizontal bounds
						enter_button_adr <= enter_icon_offset_y;
					end if;
				
					-- Player 1 was Winner ---------------------------------
					if winner = 1 then
					
						if vga_xpos >= const_winner_offset_x and vga_xpos < const_winner_offset_x + current_player_x_size and 
							vga_ypos >= const_winner_offset_y and vga_ypos < const_winner_offset_y + current_player_y_size then
							
							-- Calculate scaled positions
							player_icon_x <= (vga_xpos - const_winner_offset_x) / player_icon_scale_factor;
							player_icon_y <= (vga_ypos - const_winner_offset_y) / player_icon_scale_factor;
							
							current_player_icon1_adr <= std_logic_vector(to_unsigned(player_icon_y * pic_current_player_x + player_icon_x, current_player_icon1_adr'length));
							
							vga_red 		<= current_player_1_icon_dat(11 downto 8);
							vga_green	<= current_player_1_icon_dat( 7 downto 4);
							vga_blue		<= current_player_1_icon_dat( 3 downto 0);
							
						end if;
					
					-- Player 2 was Winner ---------------------------------
					elsif winner = 2 then
					
						if vga_xpos >= const_winner_offset_x and vga_xpos < const_winner_offset_x + current_player_x_size and 
							vga_ypos >= const_winner_offset_y and vga_ypos < const_winner_offset_y + current_player_y_size then
							
							-- Calculate scaled positions
							player_icon_x <= (vga_xpos - const_winner_offset_x) / player_icon_scale_factor;
							player_icon_y <= (vga_ypos - const_winner_offset_y) / player_icon_scale_factor;
							
							current_player_icon2_adr <= std_logic_vector(to_unsigned(player_icon_y * pic_current_player_x + player_icon_x, current_player_icon2_adr'length));
							
							vga_red 		<= current_player_2_icon_dat(11 downto 8);
							vga_green	<= current_player_2_icon_dat( 7 downto 4);
							vga_blue		<= current_player_2_icon_dat( 3 downto 0);
							
						end if;
					
					-- PC was Winner ---------------------------------------
					elsif winner = 3 then
						
						if vga_xpos >= const_winner_offset_x and vga_xpos < const_winner_offset_x + current_player_x_size and 
							vga_ypos >= const_winner_offset_y and vga_ypos < const_winner_offset_y + current_player_y_size then
							
							-- Calculate scaled positions
							player_icon_x <= (vga_xpos - const_winner_offset_x) / player_icon_scale_factor;
							player_icon_y <= (vga_ypos - const_winner_offset_y) / player_icon_scale_factor;
							
							pc_icon_adr <= std_logic_vector(to_unsigned(player_icon_y * pic_current_player_x + player_icon_x, pc_icon_adr'length));
							
							vga_red 		<= pc_icon_dat(11 downto 8);
							vga_green	<= pc_icon_dat( 7 downto 4);
							vga_blue		<= pc_icon_dat( 3 downto 0);
							
						end if;
					
					-- It was a Draw ---------------------------------------
					elsif winner = 4 then
						
						-- Check if the current pixel is within the title icon's horizontal bounds
						if vga_xpos >= draw_game_icon_x_offset and vga_xpos < draw_game_icon_x_offset + draw_game_icon_x_size then
							-- Check if the current pixel is within the title icon's vertical bounds
							if vga_ypos >= draw_game_icon_y_offset and vga_ypos < draw_game_icon_y_offset + draw_game_icon_y_size then
								-- Calculate scaled positions
								draw_game_icon_counter_x <= draw_game_icon_counter_x + 1;

								-- Update the title icon address every 'title_icon_scale_factor' pixels
								if draw_game_icon_counter_x = draw_game_icon_scale_factor then
									draw_game_icon_adr 			<= draw_game_icon_adr + 1;
									draw_game_icon_counter_x 	<= 0;
								end if;

								-- Check for the end of the row
								if vga_xpos = draw_game_icon_x_offset + draw_game_icon_x_size - 1 then
									draw_game_icon_counter_y 	<= draw_game_icon_counter_y + 1;
									draw_game_icon_counter_x 	<= 0;

									-- Update the offset every 'title_icon_scale_factor' rows
									if draw_game_icon_counter_y = draw_game_icon_scale_factor then
										draw_game_icon_counter_y <= 0;
										draw_game_icon_offset_y  <= draw_game_icon_offset_y + draw_game_icon_x;
									end if;
								end if;

								-- Set VGA output colors from the title icon data
								vga_red 		<= draw_game_icon_dat(11 downto 8);
								vga_green	<= draw_game_icon_dat( 7 downto 4);
								vga_blue		<= draw_game_icon_dat( 3 downto 0);

							else
								-- Reset vertical scaling and offset if outside title icon vertical bounds
								draw_game_icon_counter_y <= 0;
								draw_game_icon_offset_y  <= (others => '0');
							end if;
						else
							-- Reset the title icon address offset if outside title icon horizontal bounds
							draw_game_icon_adr <= draw_game_icon_offset_y;
						end if;
					
					end if;
					
				else
					trophy_scale_counter_x <= 0;	
				end if;
		end case;
		
		-- Reset ---------------------------------------------------------
		if reset = '1' then
			-- Initialization or reset related code if any
		end if;
		
	end if;	-- rising_edge(clk)

end process Graphic;
end architecture;

-- <-- end of file -->