library ieee;
use ieee.std_logic_1164.all;

package My_Package is

	type game_state 			is (Init, Start_Menu, Game_1_Player, Game_2_Player, Winner_Screen);
	type state_graphics 		is (Start_Menu, Game, Winner_Screen);
	type state_cursor_pos	is	(A0, A1, A2, B0, B1, B2, C0, C1, C2);
	type state_cursor_color	is	(red, green);
	
	constant	field_size		: integer range 0 to 15		:= 3;
	
	type field_player			is array (0 to field_size -1, 0 to field_size -1) of std_logic; 
	
	type player_turn			is (Player_1_Turn, Player_2_Turn, PC_Turn);
	
	attribute syn_encoding 									: string;
   attribute syn_encoding of game_state 				: type is "safe";
	attribute syn_encoding of state_graphics 			: type is "safe";
	attribute syn_encoding of state_cursor_pos 		: type is "safe";
	attribute syn_encoding of state_cursor_color 	: type is "safe";
	attribute syn_encoding of field_player 			: type is "safe";
	attribute syn_encoding of player_turn 				: type is "safe";

end My_Package;