# Tic-Tac-Toe

## Informations

This Tic-Tac-Toe game was created for a school project and was designed for an education board from Terasic. 		<br>
FPGA: Intel Altera Cyclone III, FBGA 484 Pins, Speed Grade 6, 15408 Logic Elements, EP3C16F454C6 					<br>

## User Manual

### Setup

1. Connect the education board with a USB-cable to a computer, where Quartus is installed.
2. Compile the project and load the bitstream on to the fpga using the onboard USB-Blaster.
3. Connect a VGA-cable to education board and the other end to a monitor.
4. Connect a PS/2 keyboard to the education board.

### How to use

In the next 3 sub chapters, there is everything explained on how to play the Tic-Tac-Toe.

#### Start Screen

In the start screen, there is a big colorful logo displayed that shows Tic-Tac-Toe.									<br>
																													<br>
There you can decide to play against the FPGA or to play a match agianst another player:

- Press any button 1 on the keyboard for singel player (against the FPGA)
- Press any button 2 on the keyboard for multi  player (against another person) 

#### Game

In the center of the game screen, there is the grid of the Tic-Tac-Toe displayed and on the left top the player who	<br>
has to make the next move. 																							<br>
*(In the single player mode against the FPGA, the player who has to make the next move never changes.)*				<br>

**Cursor Movement:**
- Move right: 	Press the right arrow key on the keyboard or the numblock key 6
- Move left:  	Press the left  arrow key on the keyboard or the numblock key 4
- Move up:		Press the up    arrow key on the keyboard or the numblock key 8
- Move down:	Press the down  arrow key on the keyboard or the numblock key 2

**Set Symbol**
- Set Symbol:	Press any enter key on the keyboard

#### Winner Screen

The winner screen will show you a trophy, if any one has won the game or the sign, that the game was a draw. 		<br>
																													<br>
To continue with another round, just press any enter key on the keyboard to get back to the start screen.
